package ee.mindborn.embeddeddemo.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlTransient;

@MappedSuperclass
public class ModelCore {

	@Id
	@GeneratedValue
	private Long dbID;

	@Column(name = "Created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Version
	private Long dbVersion;

	@Column(name = "Updated")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated;


	@XmlTransient
	public Long getDbID() {
		return dbID;
	}

	public void setDbID(Long newDbID) {
		dbID = newDbID;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date newCreated) {
		created = newCreated;
	}

	@XmlTransient
	public Long getDbVersion() {
		return dbVersion;
	}

	public void setDbVersion(Long newDbVersion) {
		dbVersion = newDbVersion;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date newUpdated) {
		updated = newUpdated;
	}

	@Override
	public String toString() {
		return "ModelCore " + " [dbID: " + getDbID() + "]" + " [created: " + getCreated() + "]" + " [dbVersion: "
				+ getDbVersion() + "]" + " [updated: " + getUpdated() + "]";
	}

	/*
	 * Compare objThat to "this" instance. If objThat is considered equal to this
	 * instance if it's the same Class and has the same database ID. This
	 * functionality is required by Vaadin classes such as Container classes which
	 * compare Model objects from the datastore.
	 */
	@Override
	public boolean equals(Object objThat) {
		// if (objThat != null) {
		// System.err.printf("ModelCore.equals: objThat class=<%s> dbid=%s\n",
		// objThat.getClass(),
		// ((ModelCore)objThat).getDbID());
		// System.err.printf("ModelCore.equals: this class=<%s> dbid=%s\n",
		// getClass(), getDbID());
		// }
		//
		// This doesn't work correctly
		//
		// if (getClass().equals(objThat.getClass()) == false)
		// return false;
		//
		// if (!(objThat instanceof ModelCore))
		// return false;
		//
		// ModelCore that = (ModelCore) objThat;
		//
		// if (getDbID() == null || that.getDbID() == null)
		// return false;
		//
		// boolean result = that.getDbID().equals(getDbID());
		// // System.err.printf("ModelCore.equals: result=%s\n", result);
		// return result;

		if (objThat == null) {
			return false;
		}

		/*
		 * Do a simple comparison by String. Note that if the class is a sub-class of
		 * something else, then the super-class info is not part of the toString(). This
		 * means we are effectually only comparing the sub-class itself and not its
		 * super-class.
		 */
		return toString().equals(objThat.toString());
	}

	/**
	 * Is objThat the same as this object? Check to see if both objects are the same database instance based on their DbID.
	 *
	 * @param objThat
	 * @return
	 */
	public boolean isSame(Object objThat) {
		if (objThat == null) {
			return false;
		}

		if (objThat instanceof ModelCore) {
			ModelCore mcThat = (ModelCore) objThat;
			if (mcThat.getDbID() != null && getDbID() != null && mcThat.getDbID() == getDbID()) {
				return true;
			} else {
				return false;
			}
		}

		return equals(objThat);
	}

	/*
	 * Custom hashCode() which uses DbID as its base.
	 */
	@Override
	public int hashCode() {
		if (this.getDbID() != null) {
			int hash = 1;
			hash = hash * 31 + this.getClass().hashCode();
			hash = hash * 31 + this.getDbID().hashCode();
			return hash;
		} else {
			return super.hashCode();
		}
	}
}

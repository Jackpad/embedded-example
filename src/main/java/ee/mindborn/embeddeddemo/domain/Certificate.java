package ee.mindborn.embeddeddemo.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Basic;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Certificate")
public class Certificate extends ModelCore {

	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "id", column = @Column(name = "CertificateId"))
	})
	private CertificateId certificateId;

	@Column(name = "Name")
	private String name;

	@Column(name = "T_Alias")
	private String alias;

	@Column(columnDefinition = "TEXT", length = 65000, name = "T_Value")
	private String value;

	@Basic
	@Column(name = "OrderID")
	private String orderID;

	@Basic
	@Column(name = "RequestedByUserName")
	private String requestedByUserName;

	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "id", column = @Column(name = "ReplacementId")),
	})
	private CertificateId replacementId;

	@Basic
	@Column(length = 5000, name = "IssuingCaNameDN")
	private String issuingCaNameDN;

	@ElementCollection(fetch = FetchType.EAGER)
	@Column(name = "T_element")
	@CollectionTable(joinColumns = {@JoinColumn(name = "Certificate_id")}, name = "Certificate_Usages")
	private Set<String> usages = new HashSet<>();

	@Column(name = "CertificatePolicyID")
	private String certificatePolicyID;

	@Column(name = "CertificateMandateID")
	private String certificateMandateID;

	@Column(name = "SerialAsHex")
	private String serialAsHex;

	@Column(name = "PurposeID")
	private String purposeID;

	@Column(name = "ValidFrom")
	@Temporal(TemporalType.TIMESTAMP)
	private Date validFrom;

	@Column(name = "ValidUntil")
	@Temporal(TemporalType.TIMESTAMP)
	private Date validUntil;

	@Column(length = 5000, name = "Subject")
	private String subject;

	@Column(length = 5000, name = "SubjectDN")
	private String subjectDN;

	@Column(name = "ForCA")
	private Boolean forCA;

	protected Certificate() {
	}

	public Certificate(CertificateId id) {
		this.certificateId = id;
	}

	public CertificateId getCertificateId() {
		return certificateId;
	}

	public void setCertificateId(CertificateId certificateId) {
		this.certificateId = certificateId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getRequestedByUserName() {
		return requestedByUserName;
	}

	public void setRequestedByUserName(String requestedByUserName) {
		this.requestedByUserName = requestedByUserName;
	}

	public CertificateId getReplacementId() {
		return replacementId;
	}

	public void setReplacementId(CertificateId replacementId) {
		this.replacementId = replacementId;
	}

	public String getIssuingCaNameDN() {
		return issuingCaNameDN;
	}

	public void setIssuingCaNameDN(String issuingCaNameDN) {
		this.issuingCaNameDN = issuingCaNameDN;
	}

	public Set<String> getUsages() {
		return usages;
	}

	public void setUsages(Set<String> usages) {
		this.usages = usages;
	}

	public String getCertificatePolicyID() {
		return certificatePolicyID;
	}

	public void setCertificatePolicyID(String certificatePolicyID) {
		this.certificatePolicyID = certificatePolicyID;
	}

	public String getCertificateMandateID() {
		return certificateMandateID;
	}

	public void setCertificateMandateID(String certificateMandateID) {
		this.certificateMandateID = certificateMandateID;
	}

	public String getSerialAsHex() {
		return serialAsHex;
	}

	public void setSerialAsHex(String serialAsHex) {
		this.serialAsHex = serialAsHex;
	}

	public String getPurposeID() {
		return purposeID;
	}

	public void setPurposeID(String purposeID) {
		this.purposeID = purposeID;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidUntil() {
		return validUntil;
	}

	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubjectDN() {
		return subjectDN;
	}

	public void setSubjectDN(String subjectDN) {
		this.subjectDN = subjectDN;
	}

	public Boolean getForCA() {
		return forCA;
	}

	public void setForCA(Boolean forCA) {
		this.forCA = forCA;
	}

	@Override
	public String toString() {
		return "Certificate " + " [serialAsHex: " + getSerialAsHex() + "]" + " [name: " + getName() + "]" + " [alias: "
				+ getAlias() + "]" + " [subject: " + getSubject() + "]" + " [subjectDN: " + getSubjectDN() + "]"
				+ " [validFrom: " + getValidFrom() + "]" + " [validUntil: " + getValidUntil() + "]" + " [forCA: "
				+ getForCA() + "]" + " [purposeID: " + getPurposeID() + "]" + " [value: " + getValue() + "]"
				+ " [orderID: " + getOrderID() + "]" + " [requestedByUserName: " + getRequestedByUserName() + "]"
				+ " [issuingCaNameDN: " + getIssuingCaNameDN() + "]" + " [issuingCaBridgeType: "
				+ " [certificatePolicyID: " + getCertificatePolicyID() + "]"
				+ " [certificateMandateID: " + getCertificateMandateID() + "]" + "{extends: " + super.toString() + "} ";
	}
}

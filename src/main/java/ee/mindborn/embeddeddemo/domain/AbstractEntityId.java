package ee.mindborn.embeddeddemo.domain;

public abstract class AbstractEntityId {

	public abstract String getId();

	protected abstract int hashOddValue();

	protected abstract int hashPrimeValue();

	public String id() {
		return getId();
	}

	@Override
	public boolean equals(Object anObject) {
		boolean equalObjects = false;

		if (anObject != null && this.getClass() == anObject.getClass()) {
			AbstractEntityId typedObject = (AbstractEntityId) anObject;
			equalObjects = this.id().equals(typedObject.id());
		}

		return equalObjects;
	}

	@Override
	public int hashCode() {
		int hashCodeValue =
				+(this.hashOddValue() * this.hashPrimeValue())
						+ this.id().hashCode();

		return hashCodeValue;
	}

	@Override
	public String toString() {
		return getId();
	}

}

package ee.mindborn.embeddeddemo.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CertificateId extends AbstractEntityId {

	@Column
	private String id;

	protected CertificateId() {
	}

	public CertificateId(String id) {
		setId(id);
	}

	public String getId() {
		return id;
	}

	public void setId(String newId) {
		id = newId;
	}

	@Override
	protected int hashOddValue() {
		return 83811;
	}

	@Override
	protected int hashPrimeValue() {
		return 263;
	}
}

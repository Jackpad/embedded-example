package ee.mindborn.embeddeddemo;

import ee.mindborn.embeddeddemo.domain.Certificate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CertificateRepository extends JpaRepository<Certificate, Long> {

	Certificate findByCertificateIdId(String id);

	@Query("from Certificate c where c.certificateId.id = :id")
	Certificate findByCertificateIdIdQuery(@Param("id") String id);

}

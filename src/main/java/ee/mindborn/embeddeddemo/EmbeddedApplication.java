package ee.mindborn.embeddeddemo;

import ee.mindborn.embeddeddemo.domain.Certificate;
import ee.mindborn.embeddeddemo.domain.CertificateId;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmbeddedApplication {

	@Autowired
	private CertificateRepository certificateRepository;

	public static void main(String[] args) {
		SpringApplication.run(EmbeddedApplication.class, args);
	}

	@PostConstruct
	public void init() {
		Certificate certificate = new Certificate(new CertificateId("some ID"));
		certificate.setName("some name");
		certificate = certificateRepository.save(certificate);
		printCertInfo(certificate);

		List<Certificate> all = certificateRepository.findAll();
		certificate = all.get(0);
		printCertInfo(certificate);

		certificate = certificateRepository.findByCertificateIdId("some ID");
		printCertInfo(certificate);

		certificate = certificateRepository.findByCertificateIdIdQuery("some ID");
		printCertInfo(certificate);

	}

	private void printCertInfo(Certificate certificate) {
		System.out.println(String.format("Id: %s, Name: %s ", certificate.getCertificateId().getId(), certificate.getName()));
		System.out.println("-------");
	}

}
